#ifndef __aidesys_gnuplot__
#define __aidesys_gnuplot__

#include "std.hpp"
#include "file.hpp"

namespace aidesys {
  /**
   * @function gnuplot
   * @memberof file
   * @static
   * @description Generated a gnuplot interface display script.
   * @param {string} file The related data file name (without extension). By contract,
   *   - the file basename is used as the plot title;
   *   - the display script is generated in `$file.gnuplot.sh`.
   *   - the plot image is generated in `$file.png`.
   *   - the data is stored in a `$file.dat`.
   *
   * To display the gnuplot, run `$file.gnuplot.sh`.
   * @param {string} plot The gnuplot display commands.
   * - see: <a href="http://gnuplot.sourceforge.net/docs_4.2/node154.html">gnuplot set commands</a>.
   * @param {string} [data = ""] An optional string with the plot data.
   */
  void gnuplot(String file, String plot, String data = "");
}
#endif
