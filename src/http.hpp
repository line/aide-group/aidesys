#ifndef __aidesys_http__
#define __aidesys_http__
#ifndef ON_WIN

#include "std.hpp"

namespace aidesys {
  /**
   * @function http
   * @class
   * @static
   * @description Performs a HTTP request and returns response.
   * - This function is accessible via the `aidesys::` prefix.
   * - It is not available in the cygwin environment.
   * @param {string} url The HTTP URL.
   * @param {string} [input=""] The URL query or content:
   * - GET method, noy used, the quety is directly appended to the URL prefixed with a '?' char and separated with '&' chars.
   * - POST method, the URL query, a string of the form _name_1=value_1&name_2=value_2_, which will be URL encoded.
   * - PUT method, the URL put content, usually using a JSON string.
   * @param {string} [method ="GET"] Either `GET`, `POST` or `PUT`, i.e. the HTTP method.
   * @return {string} The response value, or en error message if any.
   */
  std::string http(String url, String input = "", String method = "GET");
}
#endif
#endif
