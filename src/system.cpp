#include "std.hpp"
#include "regex.hpp"
// #include "SysFork.hpp"
#include "wfork.hpp"

#include <unistd.h>
#include <sys/wait.h>
#include "file.hpp"

int sys_stdlib_system(const char *command)
{
  return system(command);
}
namespace aidesys {
  std::string system(String command, bool noline, unsigned int timeout)
  {
#if 1
    std::string result = wfork([command]() { return sys_stdlib_system(command.c_str());
                               }, timeout, NULL);
#elif 1
    // This is patched, because of a spurious bug regarding fork() mechanism
    sys_stdlib_system((command + " > /tmp/sys_stdlib_system.txt").c_str());
    std::string result = load("/tmp/sys_stdlib_system.txt");
#else

    /*
     *  class ExecFork: public SysFork {
     *  std::string command;
     *  public:
     *  ExecFork(String command, unsigned int timeout) : SysFork(timeout), command(command) {}
     *  int runner()
     *  {
     *  return sys_stdlib_system(command.c_str());
     *  }
     *  }
     *  execFork(command, timeout);
     *  std::string result = execFork.run();
     */
#endif
    if(noline) {
      result = aidesys::regexReplace(aidesys::regexReplace(result, "\\s+", " "), "^\\s*([^\\s].*[^\\s])\\s*$", "$1");
    }
    return result;
  }
}
