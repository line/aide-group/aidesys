#ifndef __aidesys_std__
#define __aidesys_std__

#include <string>
#include <vector>

// Defines a string constant value.
typedef const std::string& String;

namespace aidesys {
  /**
   * @class std
   * @description Specifies standard functions of string formating, assertion verification and program line argument.
   * - These functions are accessible via the `aidesys::` prefix.
   */

  /**
   * @function echo
   * @memberof std
   * @static
   * @description Returns a formatted message à-la printf.
   * @param {string} message The message, a string format à-la printf with more parameters if used.
   * @param {...anything} parameters Any message parameters.
   * @return {string} The formated message.
   */
  std::string echo(String message, ...);

  /**
   * @function alert
   * @memberof std
   * @static
   * @description Checks a condition at run-time and throws a fatal error exception and/or dumps a warning if not verified.
   * Examples:
   * - `aidesys::alert(x == 0, "numerical-error", "division by zero");` to print a warning on `stderr` and stop the program if `x==0`.
   * - `aidesys::alert(x == 0, " numerical-error", "division by zero");` to print a warning on `stderr` without stopping the program.
   * - `aidesys::alert(verbose, "", "using aidesys::alert at %.0f", aidesys::now());` to simply echo a message if verbose.
   * @param {bool} [condition=true] If the condition is true, an alert is issued.
   * @param {string} thrown The name of the thrown exception if the alert is raised.
   * - Typical exception are:
   *   - `illegal-argument`: when a method or function receives an argument with a spurious value.
   *   - `IO-exception`:     when an input or output operation fails.
   *   - `numerical-error`:  when a numerical error occurs.
   *   - `illegal-state`:    when detecting an unexpected condition (a bug) at some point in the code.
   *     - If the name starts with a space (e.g., ` warning`) a simple warning is printed on `stderr`.
   *     - Otherwise a `aide::exception` expection is thrown.
   *     - In both case the backtrace progam stack is dumped, if compiler with `-g` option.
   * - Typical non fatal messages are:
   *   - ``:         to simply print information of `stdout`.
   *   - ` warning`: (i.e., prefixed by a space) to print a warning on `stderr`, with the program stack.
   *   - `  warning`: (i.e., prefixed by two spaces) to print a warning on `stderr`, without the program stack.
   * @param {string} message The message, a string format à-la printf with more parameters if used.
   * @param {...anything} [parameters] Any message parameters.
   * @return The condition value, unless an exception occurs.
   * @throws {exception} A `aide::exception` [std::exception](http://www.cplusplus.com/reference/exception/exception) object
   *   - The `what()` method returns `"$thrown:$message"` concatenating the expection label and related message.
   * ```
   * // For instance the following construct catches a potential exception and reformulates the message:
   * ```
   * try {
   *   aidesys::regexReplace(string, parseInput, parseOutput);
   * } catch(std::exception& e) {
   *   aidesys::alert(true, "illegal-argument", "in symboling::RecordType::RecordType bad regex syntax: '%s'", e.what());
   * }
   * ```
   */
  bool alert(unsigned int condition, String thrown, String message, ...);
  bool alert(String thrown, String message, ...);
  bool alert(unsigned int condition, String thrown);
  bool alert(String thrown);

  /**
   * @function argv
   * @memberof std
   * @static
   * @description Returns the program command line.
   * - Corresponds to the `int main(int argc, const char *argv[])` parameters.
   * - On OS not supporting the "/proc/" pseudo-filesystem (i.e., MasOS or Windows) the following construct must be used:
   * ```
   *    int main(int argc, const char *argv[]) {
   *      aidesys::argv(argc, argv);
   *      // main routine code
   *    }
   * ```
   * @return {Array<string>} A `const std::vector<std::string>` with the program command line arguments.
   */
  const std::vector < std::string >& argv(int _argc = 0, const char **_argv = NULL);

  /**
   * @function system
   * @memberof std
   * @static
   * @description Executes a system command and returns the stdout.
   * @param {string} command The command to execute. The command path must be absolute (e.g. <tt>/bin/pwd</tt> not <tt>pwd</tt>).
   * @param {bool} [noline=false] If true replaces new lines and spaces by a unique space.
   * @param {uint} [timeout=0] Optional timeout in milliseconds.
   * @return {string} The command result, or an error message.
   */
  std::string system(String command, bool noline = false, unsigned int timeout = 0);
}

#endif
