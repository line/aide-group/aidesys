@aideAPI

## Presentation

This factory encapsulates basic system interface.

- Functionalities includes:
  - Standard `std` functions for code string formatting and debugging :
    - `aidesys::echo` To format a string à-la `sprintf`s
    - `aidesys::alert` To verify an assertion and send a warning or a fatal error with trace.
    - `aidesys::argv` To get program line arguments.
    - `aidesys::system` To execute commands at the OS level.
  - Simple files `file` interface:
    - `aidesys::readDir` To returns the contents of a directory
    - `aidesys::getFileTime` and `aidesys::exists` To get the last file modification time and check if a file exists.
    - `aidesys::load` and `aidesys::save` To load/save textual files.
  - Time `time` management:
    - `aidesys::sleep` To introduce a delay.
    - `aidesys::now` To measure the real or CPU relative or absolute time.
    - `aidesys::nowISO` To get the present date and time in a standard format.
  - Regular expression (regex) and string manipulation:
    - `aidesys::regexMatch` and `aidesys::regexReplace` To match and replace strings.
    - `aidesys::regexIndexes` To get all occurrences of a regex in a string.
    - `aidesys::stringSplit` To split a string according to a regex.
  - HTTP interface via the `aidesys::http` function.
  - Python online interface via the `aidesys::python` function.
  - Pseudo-random number generation via the `aidesys::random` function.

- The implementation consists mainly of wrappers to standard third-party libraries.
