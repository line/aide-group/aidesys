#ifndef __aidesys__
#define __aidesys__

#include "std.hpp"
#include "wfork.hpp"
#include "file.hpp"
#include "gnuplot.hpp"
#include "time.hpp"
#include "regex.hpp"
#include "http.hpp"
#include "python.hpp"
#include "stats.hpp"

#endif
