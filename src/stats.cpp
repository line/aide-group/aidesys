#include "stats.hpp"

#include <math.h>
#include <cfloat>
#include <random>
#include <vector>
#include <algorithm>

#include "time.hpp"
#include "regex.hpp"
#include "file.hpp"
#include "gnuplot.hpp"

// This is used ON_WIN
#ifndef M_PI
#define M_PI 3.14159265358979323846
#endif

namespace aidesys {
  /* Implements a random generator for a given seed, using C++ mt19937_64 uniform_real_distribution. */
  class SysRandom {
    std::mt19937_64 generator;
    std::uniform_real_distribution < double > distribution;
public:
    /* Constructs the generator for a given seed. */
    SysRandom(int seed = 0) : distribution(0.0, 1.0) {
      generator.seed(seed == -1 ? (int) (1e6 * aidesys::now()) : seed);
    }
    //
    // Returns usual distributions
    //
    double getUniform()
    {
      return distribution(generator);
    }
    double getNormal()
    {
      return sqrt(-2.0 * getLoguniform()) * cos(2.0 * M_PI * getUniform());
    }
    double getGamma(unsigned int degree = 1)
    {
      double p = 0;
      for(unsigned int i = 0; i < degree; i++) {
        p -= getLoguniform();
      }
      return p;
    }
    double getLoguniform()
    {
      double r = getUniform();
      return r == 0 ? getLoguniform() : log(r);
    }
  };

  double random(char what, unsigned int degree)
  {
    static SysRandom sysRandom;
    if(what == 'u') {
      return sysRandom.getUniform();
    } else if(what == 'n') {
      return sysRandom.getNormal();
    } else if(what == 'g') {
      return sysRandom.getGamma(degree);
    } else {
      alert("illegal-argument", "in aidesys::random, unknown distribution '%c'", what);
      return NAN;
    }
  }
  std::string getStat(const double *data, unsigned int length, unsigned int *histo, unsigned int hsize, unsigned int mask)
  {
    // Calculates the bounds and momenta
    double m0 = 0, m1 = 0, m2 = 0, m3 = 0, m4 = 0, max = -DBL_MAX, min = -max;
    for(unsigned int i = 0; i < length; i++) {
      double value = data[i], value2 = value * value;
      if(!std::isnan(value)) {
        m0++, m1 += value, m2 += value2, m3 += value * value2, m4 += value2 * value2;
        min = value < min ? value : min, max = value > max ? value : max;
      }
    }
    // Count of not NAN values
    unsigned int count = (unsigned int) m0;
    // Calculates momenta related parameters
    double m = m0 == 0 ? 0 : m1 / m0, v = m0 == 1 ? 0 : (m2 - m1 * m1 / m0) / (m0 - 1), s = v < DBL_EPSILON ? 0 : sqrt(v);
    if(count == 0) {
      return echo("{ count: 0 }");
    } else {
      std::string result;
      if(mask & 0x01) {
        result += echo("\n\tcount: %.0f\n\tmin: %g\n\tmax: %g\n\tmean: %g\n\tstdev: %g\n\tskew: %g\n\tkurt: %g",
                       // - count, min, max
                       m0, min, max,
                       // - mean
                       m,
                       // - stdev
                       s,
                       // - skew
                       // n / ((n-1)(n-2)) >_i (x_i-m)^3/s^3 : http://fr.wikipedia.org/wiki/Asymétrie_(statistique)
                       (v == 0 || m0 <= 2 ? 0 : (m0 / (m0 - 1) / (m0 - 2)) *
                        // (x-m)^3/s^3
                        (m3 - 3 * m2 * m1 / m0 + 2 * pow(m1, 3) / (m0 * m0)) / pow(v, 1.5)),
                       // - kurt
                       // n (n+1) / ((n-1)(n-2)(n-3))  >_i (x_i-m)^4/s^4 - 3 * (n-1)^2/((n-2)(n-3)) : http://fr.wikipedia.org/wiki/Kurtosis
                       (v == 0 || m0 <= 3 ? 0 : -3 * (m0 - 1) * (m0 - 1) / (m0 - 2) / (m0 - 3) + m0 * (m0 + 1) / (m0 - 1) / (m0 - 2) / (m0 - 3) *
                        // >_i (x_i-m)^4/s^4
                        (m4 - 4 * m3 * m1 / m0 + 6 * m2 * m1 * m1 / m0 / m0 - 3 * pow(m1, 4) / pow(m0, 3)) / pow(v, 2)));
      }
      if(mask & 0x02) {
        result += echo("\n\tgamma-degree: %g\n\tgamma-rate: %g\n\tuniform-entropy: %g\n\tnormal-entropy: %g",
                       // - gamma-degree
                       (v <= 0 ? 0 : m * m < v ? 1 : m * m / v),
                       // - gamma-rate
                       (v <= 0 || m < 0 ? 0 : v / m),
                       // - uniform-entropy
                       (v <= 0 ? 0 : log(2 * sqrt(3) * s) / log(2)),
                       // - normal-entropy
                       (v <= 0 ? 0 : 0.5 * log(2 * M_PI * exp(1) * s) / log(2)));
      }
      // Calculates percentiles
      double inter_quartile_range;
      if(mask & (0x01 | 0x04 | 0x08)) {
        std::vector < double > vdata;
        for(unsigned int i = 0; i < length; i++) {
          if(!std::isnan(data[i])) {
            vdata.push_back(data[i]);
          }
        }
        std::sort(vdata.begin(), vdata.end());
        double *sdata = &vdata[0];
        inter_quartile_range = sdata[(3 * count) / 4] - sdata[count / 4];
        if(mask & 0x01) {
          result += echo("\n\tmedian: %g\n\tlower-quartile: %g\n\tupper-quartile: %g\n\tinter-quartile-range: %g ",
                         sdata[count / 2], sdata[count / 4], sdata[(3 * count) / 4], inter_quartile_range);
        }
      }
      // Calculates histogram values
      if(mask & (0x04 | 0x08)) {
        // Histogram size
        {
          // ref: https://www.fmrib.ox.ac.uk/datasets/techrep/tr00mj2/tr00mj2/node24.html
          double vhsize = (max - min) * pow(count, 1.0 / 3.0);
          unsigned int hsize_from_standard_deviation = s > 0 ? (int) rint(vhsize / (3.49 * s)) : 0;
          unsigned int hsize_from_inter_quartile_range = inter_quartile_range > 0 ? (int) rint(vhsize / (2 * inter_quartile_range)) : 0;
          unsigned int hsize_auto = hsize_from_standard_deviation;
          if(hsize_auto == 0 || (0 < hsize_from_inter_quartile_range && hsize_from_inter_quartile_range < hsize_from_standard_deviation)) {
            hsize_auto = hsize_from_inter_quartile_range;
          }
          hsize = hsize == 0 ? hsize_auto == 0 ? 100 : hsize_auto : hsize;
          result += echo("\n\thsize_from_standard_deviation: %d\n\thsize_from_inter_quartile_range: %d\n\thsize: %d",
                         hsize_from_standard_deviation, hsize_from_inter_quartile_range, hsize);
        }
        unsigned int *hbuffer = histo == NULL ? new unsigned int[hsize] : NULL;
        histo = histo == NULL ? hbuffer : histo;
        for(unsigned int i = 0; i < hsize; i++) {
          histo[i] = 0;
        }
        double hscale = max > min ? (double) (hsize - 1) / (max - min) : 0;
        for(unsigned int i = 0; i < length; i++) {
          if(!std::isnan(data[i])) {
            unsigned int h = (unsigned int) (hscale * (data[i] - min));
            aidesys::alert(hsize <= h, "illegal-state", "in aidesys::stat::getStat spurious histogram index h:%d for a value:%g", h, data[i]);
            histo[h]++;
          }
        }
        result += echo("\n\thscale: %g", hscale);
        // Histogram based estimations
        if(mask & 0x4) {
          // mode
          {
            unsigned int m = 0;
            for(unsigned int i = 0, hmode = 0; i < hsize; i++) {
              if(histo[i] > hmode) {
                hmode = histo[m = i];
              }
            }
            double x = m + 0.5;
            // Maximum interpolation
            if((0 < m) && (m < hsize - 1)) {
              double d1 = 0.5 * ((double) histo[m + 1] - histo[m - 1]), d2 = 2.0 * histo[m] - histo[m - 1] - histo[m + 1];
              if((0 < d2) && (fabs(d1) < d2)) {
                x += d1 / d2;
              }
            }
            double mode = min + x / hscale;
            result += echo("\n\tmode: %g", mode);
          }
          // percentiles
          {
            double percentiles[3];
            for(unsigned k = 0; k < 3; k++) {
              double v = 0.25 * (k + 1);
              unsigned int i = 0, n1 = 0, n2 = 0;
              for(; i < hsize && n2 < v * count; n1 = n2, n2 += histo[i++]) {}
              // n1 = histo[i-1] < v * count <= n2 = histo[i] // 1st order interpolation
              percentiles[k] = min + (i - 1 + (n2 > n1 ? (v * count - n1) / (n2 - n1) : 0)) / hscale;
            }
            result += echo("\n\tmedian_: %g\n\tlower-quartile_: %g\n\tupper-quartile_: %g\n\tinter-quartile-range_: %g", percentiles[1], percentiles[0], percentiles[2], percentiles[2] - percentiles[0]);
          }
          // entropy
          {
            double h = 0;
            for(unsigned int i = 0; i < hsize; i++) {
              double p = histo[i];
              if(p > 0) {
                h -= p * log(p);
              }
            }
            double entropy = (h / count + log(count / hscale)) / log(2);
            result += echo("\n\tentropy: %g", entropy);
          }
        }
        if(mask & 0x8) {
          // divergences
          int best_model = -1;
          double uniform_divergence = getDivergence(histo, [] () { return random('u');
                                                    }, count, hsize), min_divergence = uniform_divergence;
          double normal_divergence = getDivergence(histo, [] () { return random('n');
                                                   }, count, hsize);
          if(DBL_EPSILON < normal_divergence && normal_divergence < min_divergence) {
            min_divergence = normal_divergence, best_model = 0;
          }
          double gamma_1_divergence = m <= 0 ? 0 : getDivergence(histo, [] () { return random('g', 1);
                                                                 }, count, hsize);
          if(DBL_EPSILON < gamma_1_divergence && gamma_1_divergence < min_divergence) {
            min_divergence = gamma_1_divergence, best_model = 1;
          }
          double gamma_2_divergence = m <= 0 ? 0 : getDivergence(histo, [] () { return random('g', 2);
                                                                 }, count, hsize);
          if(DBL_EPSILON < gamma_2_divergence && gamma_2_divergence < min_divergence) {
            min_divergence = gamma_2_divergence, best_model = 2;
          }
          double gamma_5_divergence = m <= 0 ? 0 : getDivergence(histo, [] () { return random('g', 5);
                                                                 }, count, hsize);
          if(DBL_EPSILON < gamma_5_divergence && gamma_5_divergence < min_divergence) {
            min_divergence = gamma_5_divergence, best_model = 5;
          }
          double gamma_10_divergence = m <= 0 ? 0 : getDivergence(histo, [] () { return random('g', 10);
                                                                  }, count, hsize);
          if(DBL_EPSILON < gamma_10_divergence && gamma_10_divergence < min_divergence) {
            min_divergence = gamma_10_divergence, best_model = 10;
          }
          result += echo("\n\tuniform-divergence: %g\n\tnormal-divergence: %g\n\tgamma-1-divergence: %g\n\tgamma-2-divergence: %g\n\tgamma-5-divergence: %g\n\tgamma-10-divergence: %g\n\tbest-model: %d", uniform_divergence, normal_divergence, gamma_1_divergence, gamma_2_divergence, gamma_5_divergence, gamma_10_divergence, best_model);
        }
        delete[] hbuffer;
      }
      return "{" + result + "\n}";
    }
  }
  std::string getStat(const std::vector < double > &data, unsigned int *histo, unsigned int hsize, unsigned int mask)
  {
    return getStat(&data[0], (unsigned int) data.size(), histo, hsize, mask);
  }
  std::string getStat(std::function < double() > p, unsigned int length, unsigned int *histo, unsigned int hsize, unsigned int mask)
  {
    double data[length];
    for(unsigned int i = 0; i < length; i++) {
      data[i] = p();
    }
    return getStat(data, length, histo, hsize, mask);
  }
  double getStatValue(String name, String stat)
  {
    return strtod(aidesys::regexReplace(aidesys::regexReplace(stat, "\\s+", " "), ".* " + name + " *: *([^ ]*).*", "$1").c_str(), NULL);
  }
  double getDivergence(const unsigned int *histo1, const unsigned int *histo2, unsigned int hsize)
  {
    double div = 0, s_p = 0, s_q = 0;
    for(unsigned int i = 0; i < hsize; i++) {
      double p = histo1[i], q = histo2[i];
      if(p > 0 && q > 0) {
        div += p * log(p / q), s_p += p, s_q += q;
      }
    }
    return s_p > 0 && s_q > 0 ? (div / s_p - log(s_p / s_q)) / log(2) : 0;
  }
  double getDivergence(const unsigned int *histo1, const double *data2, unsigned int length, unsigned int hsize)
  {
    aidesys::alert(hsize == 0, "illegal-argument", "in aidesys::stats::getDivergence spurious value hsize==0");
    unsigned int histo2[hsize];
    getStat(data2, length, histo2, hsize, 0x4);
    return getDivergence(histo1, histo2, hsize);
  }
  double getDivergence(const unsigned int *histo1, const std::vector < double > &data2, unsigned int hsize)
  {
    aidesys::alert(hsize == 0, "illegal-argument", "in aidesys::stats::getDivergence spurious value hsize==0");
    unsigned int histo2[hsize];
    getStat(data2, histo2, hsize, 0x4);
    return getDivergence(histo1, histo2, hsize);
  }
  double getDivergence(const unsigned int *histo1, std::function < double() > data2, unsigned int length, unsigned int hsize)
  {
    aidesys::alert(hsize == 0, "illegal-argument", "in aidesys::stats::getDivergence spurious value hsize==0");
    unsigned int histo2[hsize];
    getStat(data2, length, histo2, hsize, 0x4);
    return getDivergence(histo1, histo2, hsize);
  }
  double getDivergence(const double *data1, const unsigned int *histo2, unsigned int length, unsigned int hsize)
  {
    aidesys::alert(hsize == 0, "illegal-argument", "in aidesys::stats::getDivergence spurious value hsize==0");
    unsigned int histo1[hsize];
    getStat(data1, length, histo1, hsize, 0x4);
    return getDivergence(histo1, histo2, hsize);
  }
  double getDivergence(const std::vector < double > &data1, const unsigned int *histo2, unsigned int hsize)
  {
    aidesys::alert(hsize == 0, "illegal-argument", "in aidesys::stats::getDivergence spurious value hsize==0");
    unsigned int histo1[hsize];
    getStat(data1, histo1, hsize, 0x4);
    return getDivergence(histo1, histo2, hsize);
  }
  double getDivergence(std::function < double() > data1, const unsigned int *histo2, unsigned int length, unsigned int hsize)
  {
    aidesys::alert(hsize == 0, "illegal-argument", "in aidesys::stats::getDivergence spurious value hsize==0");
    unsigned int histo1[hsize];
    getStat(data1, length, histo1, hsize, 0x4);
    return getDivergence(histo1, histo2, hsize);
  }
  double getDivergence(const double *data1, const double *data2, unsigned int length, unsigned int hsize)
  {
    unsigned int histo1[length], histo2[length];
    std::string s = getStat(data1, length, histo1, hsize, 0x4);
    getStat(data2, length, histo2, hsize = (unsigned int) getStatValue("hsize", s), 0x4);
    return getDivergence(histo1, histo2, hsize);
  }
  double getDivergence(const double *data1, const std::vector < double > &data2, unsigned int hsize)
  {
    return getDivergence(data1, &data2[0], data2.size(), hsize);
  }
  double getDivergence(const double *data1, std::function < double() > p2, unsigned int length, unsigned int hsize)
  {
    double data2[length];
    for(unsigned int i = 0; i < length; i++) {
      data2[i] = p2();
    }
    return getDivergence(data1, data2, length, hsize);
  }
  double getDivergence(const std::vector < double > &data1, const double *data2, unsigned int hsize)
  {
    return getDivergence(&data1[0], data2, (unsigned int) data1.size(), hsize);
  }
  double getDivergence(const std::vector < double > &data1, const std::vector < double > &data2, unsigned int hsize)
  {
    if(data1.size() == data2.size()) {
      return getDivergence(&data1[0], &data2[0], (unsigned int) data1.size(), hsize);
    } else {
      unsigned int histo1[data1.size()], histo2[data2.size()];
      std::string s = getStat(data1, histo1, hsize, 0x4);
      getStat(data2, histo2, hsize = (unsigned int) getStatValue("hsize", s), 0x4);
      return getDivergence(histo1, histo2, hsize);
    }
  }
  double getDivergence(const std::vector < double > &data1, std::function < double() > p2, unsigned int hsize)
  {
    unsigned int length = data1.size();
    double data2[length];
    for(unsigned int i = 0; i < length; i++) {
      data2[i] = p2();
    }
    return getDivergence(&data1[0], data2, length, hsize);
  }
  double getDivergence(std::function < double() > p1, const double *data2, unsigned int length, unsigned int hsize)
  {
    double data1[length];
    for(unsigned int i = 0; i < length; i++) {
      data1[i] = p1();
    }
    return getDivergence(data1, data2, length, hsize);
  }
  double getDivergence(std::function < double() > p1, const std::vector < double > &data2, unsigned int hsize)
  {
    unsigned int length = data2.size();
    double data1[length];
    for(unsigned int i = 0; i < length; i++) {
      data1[i] = p1();
    }
    return getDivergence(data1, &data2[0], length, hsize);
  }
  double getDivergence(std::function < double() > p1, std::function < double() > p2, unsigned int length, unsigned int hsize)
  {
    double data1[length], data2[length];
    for(unsigned int i = 0; i < length; i++) {
      data1[i] = p1(), data2[i] = p2();
    }
    return getDivergence(data1, data2, length, hsize);
  }
  class KnownDensities {
    class Density {
public:
      virtual double p(double x) const
      {
        aidesys::alert(true, "illegal-state", "in aidesys::stats::KnownDensities::Density the p() function is not implemented");
        return NAN;
      }
      virtual ~Density() {}
    };
    class UniformDensity: public Density {
      double min, max;
public:
      UniformDensity(double mean, double stdev) : min(mean - sqrt(3) * stdev), max(mean + sqrt(3) * stdev) {}
      UniformDensity(double min, double max, bool using_bounds) : min(min), max(max) {}
      virtual double p(double x) const
      {
        return min < max ? 1 / (max - min) : 0;
      }
    };
    class NormalDensity: public Density {
      double m, s, k;
public:
      NormalDensity(double mean, double stdev) : m(mean), s(stdev), k(s * sqrt(2.0 * M_PI)) {}
      virtual double p(double x) const
      {
        if(DBL_EPSILON < s) {
          double u = (x - m) / s;
          return exp(-0.5 * u * u) / k;
        } else {
          return 1;
        }
      }
    };
    class GammaDensity: public Density {
      double d, t, k;
public:
      GammaDensity(double mean, double stdev) : GammaDensity(stdev < DBL_EPSILON || mean * mean < stdev * stdev ? 1 : mean * mean / (stdev * stdev), mean < DBL_EPSILON ? 0 : stdev * stdev / (mean * mean), true) {}
      GammaDensity(double degree, double rate, bool using_parameters) : d(degree), t(rate), k(t * tgamma(d)) {}
      virtual double p(double x) const
      {
        return 0 < t ? pow(x / t, d - 1) * exp(-x / t) / k : 1;
      }
    };
    const Density *density;
public:
    KnownDensities(String stat, char name) :
      density(name == 'g' ? (Density *) new GammaDensity(getStatValue("mean", stat), getStatValue("stdev", stat)) :
              name == 'n' ? (Density *) new NormalDensity(getStatValue("mean", stat), getStatValue("stdev", stat)) :
              (Density *) new UniformDensity(getStatValue("mean", stat), getStatValue("stdev", stat))) {}
    KnownDensities(String stat) : KnownDensities(stat, getStatValue("best-model", stat) == 0 ? 'n' : getStatValue("best-model", stat) > 0 ? 'g' : 'u') {}
    ~KnownDensities() {
      delete density;
    }
    double p(double x) const
    {
      return density->p(x);
    }
  };
  void plotHistogram(String file, String stat, const unsigned int *histo, bool model)
  {
    KnownDensities density(stat);
    double min = getStatValue("min", stat), hscale = getStatValue("hscale", stat), count = getStatValue("count", stat);
    unsigned int hsize = getStatValue("hsize", stat);
    std::string dat;
    for(unsigned int i = 0; i < hsize; i++) {
      double x = min + (i + 0.5) / hscale;
      dat += model ?
             echo("%g %g %g\n", x, hscale * histo[i] / count, density.p(x)) :
             echo("%g %g\n", x, hscale * histo[i] / count);
    }
    std::string plot = "set title \"" + file + (model ? (std::string) " as " + (getStatValue("best-model", stat) == 0 ? "normal" : getStatValue("best-model", stat) > 0 ? "gamma" : "uniform") : "") + "\"\nset yrange [0:*] writeback\nplot \"" + file + ".dat\" using 1:2 with boxes linecolor \"black\" notitle" + (model ? ", \"" + file + ".dat\" using 1:3 with lines linecolor \"red\" linewidth 2 notitle\n" : "\n");
    gnuplot(file, plot, dat);
  }
  void plotStatCurve(String file, const std::vector < double > &means, const std::vector < double > &stdevs, double x0, double x1)
  {
    // Ref : https://www.cs.hmc.edu/~vrable/gnuplot/using-gnuplot.html
    std::string dat;
    double x = x0, dx = x1 < x0 ? 1 : means.size() > 1 ? (x1 - x0) / (means.size() - 1) : 0;
    for(unsigned int i = 0; i < means.size(); i++, x += dx) {
      dat += echo("%g %g %g\n", x, means.at(i), i < stdevs.size() ? stdevs.at(i) : 0);
    }
    gnuplot(file, "plot \"" + file + ".dat\" using 1:2:3 with line linecolor \"green\" notitle , \"" + file + ".dat\" using 1:2:3 with yerrorbars linecolor \"red\" notitle", dat);
  }
  void plotStatCurve(String file, const std::vector < double > &means, double x0, double x1)
  {
    std::vector < double > stdevs;
    plotStatCurve(file, means, stdevs, x0, x1);
  }
  void plotStatCurve(String file, const std::vector < std::string > &stats, double x0, double x1)
  {
    std::vector < double > means, stdevs;
    for(auto it = stats.begin(); it != stats.end(); it++) {
      means.push_back(getStatValue("mean", *it)), stdevs.push_back(getStatValue("stdev", *it));
    }
    plotStatCurve(file, means, stdevs, x0, x1);
  }
  void plotStatBoxes(String file, const std::map < std::string, std::string > &stats)
  {
    // Ref: https://stackoverflow.com/questions/15404628/how-can-i-generate-box-and-whisker-plots-with-variable-box-width-in-gnuplot
    std::string dat;
    unsigned int k = 0;
    for(auto it = stats.begin(); it != stats.end(); it++, k++) {
      dat += echo("%d %g %g %g %g %g %g %s\n", k, getStatValue("min", it->second), getStatValue("lower-quartile", it->second), getStatValue("median", it->second), getStatValue("upper-quartile", it->second), getStatValue("max", it->second), 0.5, it->first.c_str());
    }
    gnuplot(file, "set style fill empty\nplot \"" + file + ".dat\" using 1:3:2:6:5:7:xticlabels(8) with candlesticks  whiskerbars linecolor \"red\" lt 3 notitle, '' using 1:4:4:4:4:7 with candlesticks lt -1 notitle", dat);
  }
}
