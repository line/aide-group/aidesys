#ifndef __aidesys_stats__
#define __aidesys_stats__
#include "random.hpp"

#include "std.hpp"
#include <vector>
#include <map>
#include <functional>

namespace aidesys {
  /**
   * @class stats
   * @description Specifies some utility for statistics.
   * - These functions are accessible via the `aidesys::` prefix.
   */
  /**
   * @function random
   * @memberof stats
   * @static
   * @description Returns a deterministic pseudo-random number.
   * - This function is accessible via the `aidesys::` prefix.
   * @param {char} [what='i'] Random distribution.
   * - 'u', "uniform": drawn from a uniform distribution in [0, 1] (degree is not used).
   * - 'n', "normal":  drawn from a Gaussian normal distribution (degree is not used).
   * - 'g', "gamma":   drawn from a Gamma distribution, of a given degree.
   * @param {uint} [degree=1] The Gamma distribution degree, usually 1, 2, 5 or 10.
   * @return {double} A random number.
   */
  /**
   * @function getStat
   * @memberof stats
   * @static
   * @description Computes usual 1D statistics returned as a parsable weak json string.
   *
   * _0X1_ As a function of the sample momenta:
   * - _"count"_: the number of observed values.
   * - _"min"_: the observed minimal value.
   * - _"max"_: the observed maximal value.
   * - _"mean"_: the mean value.
   * - _"stdev"_: the standard-deviation. (2nd order).
   *   - Returns an unbiased estimator of the theoretical standard-deviation `E[(x-m)^2]^(1/2)` (Any standard-deviation lower than DBL_EPSILON (1e-15) is set to 0).
   * - _"skew"  : the skewness (3rd order).
   *   -  Returns an estimator of the theoretical skewness <a href="http://en.wikipedia.org/wiki/Skewness"> skewness</a> `E[(x - m)^3 / stdev^3]` (Skewness is zero for symmetric distributions).
   * - _"kurt"  : the kurtosis (4th order).
   *   - An estimator of the theoretical kurtosis <a href="http://en.wikipedia.org/wiki/Kurtosis">kurtosis</a> `E[(x - m)^4 / stdev^4] - 3` (Kurtosis is equal to 0 for the Gaussian distribution).
   *
   * _0X2_ As a function of the sample momenta, with respect to usual distributions:
   * - _"gamma-degree"_: the degree of a Gamma distribution, with the same mean and variance as the empirical distribution.
   *   - Returns the degree `d >= 1` or `0` if undefined, assuming that the probability distribution has the form <br/>`p(t) = (t / tau)^(d - 1) exp(-t / tau) / (tau d!)` e.g., `d = 1` for a Poisson distribution.
   * - _"gamma-rate"   : the rate of a Gamma distribution, with the same mean and variance as the empirical distribution.
   *   - Returns the rate `tau >= 1` or `0` if undefined, assuming a Gamma distribution as above.
   * - _"uniform-entropy"_: the entropy of a uniform distribution with the same mean and variance.
   * - _"normal-entropy"_: the entropy of a Gaussian distribution with the same mean and variance.
   *
   * _0X4_ As a function of the histogram:
   * - _"hsize"_: the histogram sampling size.
   * - _"hsize_from_standard_deviation"_: the histogram sampling size, according to [(Scott 1979)](https://www.fmrib.ox.ac.uk/datasets/techrep/tr00mj2/tr00mj2/node24.html).
   * - _"hsize_from_inter_quartile_range_: the histogram sampling size, according to [(Izenman 1991)](https://www.fmrib.ox.ac.uk/datasets/techrep/tr00mj2/tr00mj2/node24.html).
   * - _"mode"_: the mode value, i.e. the most frequent value, with 2nd order interpolation.
   * - _"median", _"lower-quartile", _"upper-quartile"_, _"inter-quartile-range"_: The median corresponds to the percentile for at 50%. The quartiles to the percentile at 25% and 75%. Values are calculated from the histogram with 1st order interpolation.
   * - _"median_", _"lower-quartile_", _"upper-quartile_"_, _"inter-quartile-range_"_: Here, values are calculated from the sorted data.
   * - _"entropy"_: The distribution entropy, in bits.
   *   - We use the approximation: <br>`-sum_i p_i log_2(p_i) + log_2(epsilon)`,<br> where `p_i` is the probability of the `i`-th box of size `epsilon`.
   *
   * _0X8_ As a function of the histogram, with respect to usual distributions:
   * - _"uniform-divergence"_: The Kullback-Leibler divergence between the empirical distribution and a uniform distribution having the same mean and variance.
   * - _"normal-divergence"_: The Kullback-Leibler divergence between the empirical distribution and a Gaussian distribution having the same mean and variance.
   * - _"gamma-1-divergence"_: The Kullback-Leibler divergence between the empirical distribution and a Gamma distribution of degree `1` having the same mean and variance.
   * - _"gamma-2-divergence"_: The Kullback-Leibler divergence between the empirical distribution and a Gamma distribution of degree `2` having the same mean and variance.
   * - _"gamma-5-divergence"_: The Kullback-Leibler divergence between the empirical distribution and a Gamma distribution of degree `5` having the same mean and variance.
   * - _"gamma-10-divergence"_: The Kullback-Leibler divergence between the empirical distribution and a Gamma distribution of degree `10` having the same mean and variance.
   * - _"best-model"_: Using the following numerical code:
   * `0`: "normal",
   * `1`, `2`, `5, `10`: "gamma-1", "gamma-2", "gamma-5", "gamma-10",
   * `-1`: "uniform" as a fallback option.
   *
   * @param {Array|Vector|Function} data The 1D data.
   * - It can be provided as a `double[]` array  or a `std::vector<double>` buffer.
   * - It also can be given as `double data()` random function or as a lambda: <br/>`aidesys::getStat([] () { return aidesys::random('g', 10); }, 1000);`
   * @param {uint} length The data length, if given as an array or density function.
   * @param {Array} [histo] An optional `unsigned int[]` histogram buffer to return histogram values between min and max.
   * - The used size is always below the data `length`.
   * @param {uint} [hsize=0] An optional histogram size to compute histogram based values between min and max. If not specified but the mask includes estimation based on an histogram, an optimal size is calculated as the maximum between the  [(Scott 1979)](https://www.fmrib.ox.ac.uk/datasets/techrep/tr00mj2/tr00mj2/node24.html) and [(Izenman 1991)](https://www.fmrib.ox.ac.uk/datasets/techrep/tr00mj2/tr00mj2/node24.html) estimation, with a fallback to 100, if estimations fails.
   * @param {uint} [mask= 0xF] An optional mask to deselect some calculation:
   * - _0X1_ As a function of the sample momenta.
   * - _0X2_ As a function of the sample momenta, with respect to usual distributions.
   * - _0X4_ As a function of the histogram.
   * - _0X8_ As a function of the histogram, with respect to usual distributions.
   * @return {string} The 1D statistics parameters returned as a parsable JSON string.
   * - The following construct allows to extract a given parameter:<br/>
   * `[getStatValue](.#getStatValue)(name, getStat(../..))`
   */
  std::string getStat(const double *data, unsigned int length, unsigned int *histo = NULL, unsigned int hsize = 0, unsigned int mask = 0xF);
  std::string getStat(const std::vector < double > &data, unsigned int *histo = NULL, unsigned int hsize = 0, unsigned int mask = 0xF);
  // - Better than std::string getStat(double (*data)(), unsigned int length, etc…);
  std::string getStat(std::function < double() > data, unsigned int length, unsigned int *histo = NULL, unsigned int hsize = 0, unsigned int mask = 0xF);

  /**
   * @function getStatValue
   * @memberof stats
   * @static
   * @description Extract a getStat value from the JSON string.
   * @param {string} name The parameter name.
   * @param {string} stat The [getStat](#.getStat) string returned value.
   * @param {double} The related parameter value, or `NAN` if not a number.
   */
  double getStatValue(String name, String stat);

  /**
   * @function getDivergence
   * @memberof stats
   * @static
   * @description Calculates the Kullback-Leibler divergence between this empirical distribution and another probality density.
   *   - The <a href="http://en.wikipedia.org/wiki/Kullback-Leibler_divergence">Kullback–Leibler divergence</a> `d(p||q) = >_w p ln(p/q)`, where `p` is the empirical distribution and `q` the model distribution corresponds to the average number of bits difference when coding `p` with `q`.
   * @param {Histogram|Array|Vector|Function} data1 The 1D data empirical data.
   * - It can be provided as a `unsigned int[]` array corresponding to the data histogram.
   * - It also can be provided as a `double[]` array  or a `std::vector<double>` buffer or `double p()` random function or lambda.
   * @param {Histogram|Array|Vector|Function} data2 The 1D model distribution data.
   * - It can be provided as a `unsigned int[]` array corresponding to the data histogram.
   * - It alo can be provided as a `double[]` array  or a `std::vector<double>` buffer or `double p()` random function or lam   * @param {uint} length The data length, if given as arrays or density functions.
   * @param {uint} hsize The histogram size.
   * @return The Kullback-Leibler divergence in bits.
   */

  double getDivergence(const unsigned int *histo1, const unsigned int *histo2, unsigned int hsize);
  double getDivergence(const unsigned int *histo1, const double *data2, unsigned int length, unsigned int hsize);
  double getDivergence(const unsigned int *histo1, const std::vector < double > &data2, unsigned int hsize);
  double getDivergence(const unsigned int *histo1, std::function < double() > data2, unsigned int length, unsigned int hsize);
  double getDivergence(const double *data1, const unsigned int *histo2, unsigned int length, unsigned int hsize);
  double getDivergence(const std::vector < double > &data1, const unsigned int *histo2, unsigned int hsize);
  double getDivergence(std::function < double() > data1, const unsigned int *histo2, unsigned int length, unsigned int hsize);
  double getDivergence(const double *data1, const double *data2, unsigned int length, unsigned int hsize = 0);
  double getDivergence(const double *data1, const std::vector < double > &data2, unsigned int hsize = 0);
  double getDivergence(const double *data1, std::function < double() > data2, unsigned int length, unsigned int hsize = 0);
  double getDivergence(const std::vector < double > &data1, const double *data2, unsigned int hsize = 0);
  double getDivergence(const std::vector < double > &data1, const std::vector < double > &data2, unsigned int hsize = 0);
  double getDivergence(const std::vector < double > &data1, std::function < double() > data2, unsigned int hsize = 0);
  double getDivergence(std::function < double() > data1, const double *data2, unsigned int length, unsigned int hsize = 0);
  double getDivergence(std::function < double() > data1, const std::vector < double > &data2, unsigned int hsize = 0);
  double getDivergence(std::function < double() > data1, std::function < double() > data2, unsigned int length, unsigned int hsize = 0);

  /**
   * @function plotHistogram
   * @memberof stats
   * @static
   * @description Plots an histogram, as exemplified here:
   * <center><img height='300' src='statsdemo/normal0.png'></center>
   * @param {string} file The plot base name, storing in `$file(.png|.dat|.gnuplot.sh)`, as documented for [gnuplot](./file.html#.gnuplot).
   * @param {string} stat A string with the data statistics as provided by [`getStat`](#getStat).
   * @param {Array} histo The histogram buffer which size id defined by `getStatValue("hsize", stat)`.
   * @param {bool} model If true also draw the best model adjustment.
   */
  void plotHistogram(String file, String stat, const unsigned int *histo, bool model = false);

  /**
   * @function plotStatCurve
   * @memberof stats
   * @static
   * @description Plots means and standard deviations as a curve, as exemplified here:
   * <center><img height='300' src='statsdemo/normal1.png'></center>
   * @param {string} file The plot base name, storing in `$file(.png|.dat|.gnuplot.sh)`, as documented for [gnuplot](./file.html#.gnuplot).
   * @param {Array} means A `std::vector<double>` of mean values.
   * - A `std::vector<std::string>` of [getStat](#.getStat) strings.
   * @param {Array} [stdevs] An optional `std::vector<double>` of standard-deviation values.
   * - If ommited a simple curve is drawn.
   * @param {double} [x0=0] The abcissa of means.at(0);
   * @param {double} [x1=-1] The abcissa of means.at(means.size()-1)., The `-1` value stands for `x1 = means.size()-1`.
   */
  void plotStatCurve(String file, const std::vector < double > &means, const std::vector < double > &stdevs, double x0 = 0, double x1 = -1);
  void plotStatCurve(String file, const std::vector < double > &means, double x0 = 0, double x1 = -1);
  void plotStatCurve(String file, const std::vector < std::string > &stats, double x0 = 0, double x1 = -1);

  /**
   * @function plotStatBoxes
   * @memberof stats
   * @static
   * @description Plots means and standard deviations as statistical boxes, as exemplified here:
   * <center><img height='300' src='statsdemo/normal2.png'></center>
   * @param {string} file The plot base name, storing in `$file(.png|.dat|.gnuplot.sh)`, as documented for [gnuplot](./file.html#.gnuplot).
   * @param {Array} An `std::map<std::string, std::string >` of named [getStat](#.getStat) strings, i.e., [getStat](#.getStat) strings indexed by names.
   */
  void plotStatBoxes(String file, const std::map < std::string, std::string > &stats);
}
#endif
