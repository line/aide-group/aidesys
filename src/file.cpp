#include "file.hpp"

#include <stdio.h>
#include <string.h>
#include <libgen.h>
#include <sys/stat.h>

#include <math.h>
#include "regex.hpp"

#include <limits.h>
#include <stdlib.h>

// Used by aidesys::readDdir
#include <dirent.h>

int sys_stdlib_system(const char *command);

namespace aidesys {
  const std::vector < std::string > readDir(String path, String pattern) {
    std::vector < std::string > list;
    DIR *dr = opendir(path.c_str());
    aidesys::alert(dr == NULL, "illegal-argument", "in aidesys::readDir, '" + path + "' is not a valid directory");
    for(struct dirent *de; (de = readdir(dr)) != NULL;) {
      std::string file = de->d_name;
      if(regexMatch(file, pattern)) {
        list.push_back(file);
      }
    }
    closedir(dr);
    return list;
  }

  double getFileTime(String filename)
  {
    struct stat statbuf;
    if(stat(filename.c_str(), &statbuf) == 0) {
#ifdef ON_MAC
      return 1e+3 * statbuf.st_ctimespec.tv_sec;
#else
      return 1e+3 * statbuf.st_ctim.tv_sec;
#endif
    } else {
      return NAN;
    }
  }
  class Path {
public:
#if ON_WIN
    const static char delim = '\\';
#else
    const static char delim = '/';
#endif
    static std::string get_basename(String filename)
    {
      auto i = filename.find_last_of(delim);
      return i == std::string::npos ? filename : filename.substr(i + 1);
    }
    static std::string get_dirname(String filename)
    {
      auto i = filename.find_last_of(delim);
      return i == std::string::npos ? "." : filename.substr(0, i + 1);
    }
    static std::string remove_extension(String filename)
    {
      auto j = filename.find_last_of('.');
      return j == std::string::npos ? filename : filename.substr(0, j);
    }
    static std::string get_extension(String filename)
    {
      auto j = filename.find_last_of('.');
      return j == std::string::npos ? filename : filename.substr(j + 1);
    }
    static std::string get_normalized(String filename)
    {
      std::string result = filename, result0 = "";
      result = aidesys::regexReplace(result, "/+", "/");
      result = aidesys::regexReplace(aidesys::regexReplace(result, "^\\./", ""), "([^\\.])\\./", "$1");
      do {
        result = aidesys::regexReplace(result0 = result, "[^/]+/\\.\\./", "");
      } while(result != result0);
      return result;
    }
    static std::string get_realpath(String filename)
    {
      char r[PATH_MAX + 1] = "";
      char *o = realpath(filename.c_str(), r);
      if(o != NULL) {
        return std::string(r);
      }
      if(filename.at(0) != '/') {
        realpath(".", r);
        std::string result(r);
        if(o != NULL) {
          return get_normalized(result + "/" + filename);
        }
      }
      return get_normalized(filename);
    }
  };
  std::string path(String filename, char what)
  {
    switch(what) {
    case 'b':
      return Path::remove_extension(Path::get_basename(filename));
    case 'f':
      return Path::get_basename(filename);
    case 'd':
      return Path::get_dirname(filename);
    case 'e':
      return Path::get_extension(filename);
    case 'n':
      return Path::get_normalized(filename);
    case 'r':
      return Path::get_realpath(filename);
    default:
      alert(true, "illegal-argument", "in aidesys::file::path undefined what: '%c' argument", what);
      return filename;
    }
  }
  bool exists(String filename)
  {
    struct stat statbuf;
    return stat(filename.c_str(), &statbuf) == 0;
  }
  std::string load(String filename)
  {
    std::string string;
    FILE *fp = fopen(filename.c_str(), "r");
    alert(fp == NULL, "io-exception", "in aidesys::load, unable to open '" + filename + "'");
    for(char c; (c = (char) fgetc(fp)) != (char) EOF;) {
      string += c;
    }
    alert(ferror(fp) != 0, "io-exception", "in aidesys::load, error '%s' when loading in '" + filename + "'", strerror(ferror(fp)));
    fclose(fp);
    return string;
  }
  void save_backup(String filename)
  {
    if(exists(filename)) {
      std::string f = filename + "~";
      save_backup(f);
      rename(filename.c_str(), f.c_str());
    }
  }
  void save(String filename, String string)
  {
    // Creates the directory if required
    for(size_t it = 0; (it = filename.find('/', it + 1)) != std::string::npos;) {
      mkdir(filename.substr(0, it).c_str(), 0777);
    }
    // Creates backup file if required
    save_backup(filename);
    // Saves the file
    FILE *fp = fopen(filename.c_str(), "w");
    alert(fp == NULL, "io-exception", "in aidesys::save, unable to open '" + filename + "'");
    fprintf(fp, "%s", string.c_str());
    alert(ferror(fp) != 0, "io-exception", "in aidesys::save, error '%s' when saving in '" + filename + "'", strerror(ferror(fp)));
    fclose(fp);
  }
}
