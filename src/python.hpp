#ifndef __aidesys_python__
#define __aidesys_python__
#ifndef ON_WIN
#ifndef SWIG

#include "std.hpp"

#define PY_SSIZE_T_CLEAN
#include <Python.h>

namespace aidesys {
  /**
   * @function python
   * @class
   * @static
   * @description Starts/stops a Python session or runs a command.
   * - This function is accessible via the `aidesys::` prefix.
   * - It is not available in the cygwin environment.
   *
   * - To gets a value from the python session, you may use a code of the form:
   * ```
   *   aidesys::python("start");
   *   aidesys::python("v = 2 + 3");
   *   std::string r = aidesys::python("get v"); // => r == "5"
   *   aidesys::python("stop");
   * ```
   *
   * - The `aidesys::python("command1"); aidesys::python("command2"); ...` sequence is equivalent to the following construct:
   *```
   *   #include <Python.h>
   *   ../..
   *   int err = PyRun_SimpleString("command1" "command2");
   *   aidesys::alert(err != 0, "illegal-state", ..
   *```
   *
   * @param {string} what Either
   * - `start`: starts the wrapping
   * - `stop`: stops the wrapping.
   * - `get value`: returns a python variable as a string, as if printed via the python `print()` command.
   * - or any command to execute.
   * @param {uint} [timeout=0] Optional timeout in milliseconds.
   */
  std::string python(String what, unsigned int timeout = 0);
}
#endif
#endif
#endif
