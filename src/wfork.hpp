#ifndef __aidesys_wfork__
#define __aidesys_wfork__

#include "std.hpp"
#include <functional>

namespace aidesys {
  /**
   * @function wfork
   * @memberof std
   * @static
   * @description Implements the execution of a runner in a child process.
   * - The function corresponds to [setTimeout() and setInterval()](https://www.w3schools.com/js/js_timing.asp) javascript timing methods.
   * - Its [implementation](https://gitlab.inria.fr/line/aide-group/aidesys/-/raw/master/src/wfork.cpp) is quite straightforward and provides a very basic example of using the [C/C++ fork](https://www.geeksforgeeks.org/fork-system-call) mechanism.
   * - The method returns immediatly.
   * - The child process always exit with a '0' error code.
   * - All childs processes are properly killed when program stops.
   * @param {Function} runner A `int()` function that returns a status and print on stdout.
   * @param {char} what Either:
   * - 't' for the setTimeout mechanism, i.e, executes a the runner, after waiting a specified number of milliseconds (this is the default behavior).
   * - 'i' for the setInterval mechanism, i.e, repeats the execution of the runner continuously, after waiting a 1st delay, while the `runner()` return '0' (no error).
   * @param {uint} delay The timeout or interval delay in millisecond.
   * @return {pid} The runner process [pid](https://ftp.gnu.org/old-gnu/Manuals/glibc-2.2.3/html_node/libc_554.html):
   * - It allows to send a signal to the child process, using `wfork_kill(pid);`
   */
  int wfork(std::function < int() > runner, char what, double delay);
  void wfork_kill(int pid); // documented in wfork(.)
  void wfork_abort(bool condition, String message); // Internal routine to manage errors: print message and abort
  void wfork_register(int pid, bool add_else_del); // Internal routine to clean child processes at program end

  /**
   * @function wfork
   * @memberof std
   * @static
   * @description Implements the execution of a runner in a child process, returning the stdout.
   * - The function is called with prefix `aidesys::wfork(·)`.
   * - Typical usage:
   * ```
   * struct MyClass {
   *  // Runner returned status
   *  unsigned int status;
   *  // Here the runner is an instance method, with some parameter.
   *  int runner(String name) { cout << "Hello " + name + "!"; return 0; }
   *  // Calls the runner in a child process with timeout
   *  std::string run(String name, unsigned int timeout) {
   *    // The lambda construction allows to use it capturing the related parameters.
   *    return aidesys::wfork([this, name]() { return runner(name); }, timeout, &status);
   * ../..
   * ```
   * @param {Function} runner A `int()` function that returns a status and print on stdout.
   * @param {uint} [timeout = 0] An optional timeout value, in millisecond. No timeout if the value is `0`.
   * @param {uint} [status = NULL] An optional `unsigned int *status` pointer to get the runner return status:
   * - `0` : if no error;
   * - `256`: if in timeout;
   * - otherwise, an error code between 1 and 255.
   * @return {string} The runner process sdtout.
   * - The method returns after the runner process ends or is on timeout.
   */
  std::string wfork(std::function < int() > runner, unsigned int timeout = 0, unsigned int *status = NULL);
}

#endif
