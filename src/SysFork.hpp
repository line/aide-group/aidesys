#ifndef __aidesys_sysfork__
#define __aidesys_sysfork__

#include "std.hpp"

namespace aidesys {
  //
  // THIS CLASS IS DEPRECATED => wfork() is used instead
  //

  /* Implements the execution of a command in a child process, returning the stdout. */
  class SysFork {
    unsigned int timeout = 0, status = 1024;
public:
    /* Constructs a fork mechanism with an optional timeout. */
    SysFork(unsigned int timeout = 0) : timeout(timeout) {}
    virtual ~SysFork() {}
    /* Defines the command to fork, returning a status. */
    virtual int runner();
    /* Executes the command in child process and returns the stdout. */
    std::string run();
    /* Returns the runner status or 256 if timeout. */
    unsigned int getStatus()
    {
      return status;
    }
  };
}

#endif
