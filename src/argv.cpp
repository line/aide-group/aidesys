#include "std.hpp"
#include <string.h>

namespace aidesys {
  const std::vector < std::string >& argv(int _argc, const char **_argv) {
    static std::vector < std::string > argv;
    if(argv.size() == 0) {
#ifndef ON_MAC
      char cmdline[4096];
      FILE *fp = fopen("/proc/self/cmdline", "r");
      fgets(cmdline, 4096, fp);
      fclose(fp);
      for(char *s = cmdline; *s != '\0'; s += strlen(s) + 1) {
        argv.push_back(s);
      }
#else
      if(_argc > 0) {
        for(int i = 0; i < _argc; i++) {
          argv.push_back(_argv[i]);
        }
      } else {
        // Here we are in the configuration of a node Napi wrapping.
        argv.push_back("node");
      }
#endif
    }
    return argv;
  }
}
