#include "time.hpp"

#include <time.h>
#include <math.h>
#include <cmath>
#include <sys/time.h>
#include <sys/resource.h>

namespace aidesys {
  void sleep(unsigned int delay)
  {
    struct timespec ts;
    ts.tv_sec = delay / 1000;
    ts.tv_nsec = (delay % 1000) * 1000000;
    nanosleep(&ts, NULL);
  }
  double now(bool daytime, bool relative)
  {
    if(daytime) {
      static double t0 = NAN;
      struct timeval time;
      alert(gettimeofday(&time, NULL) != 0, "illegal-state", "in aidesys::now, bad gettimeofday() system call");
      double t = 1e+3 * time.tv_sec + 1e-3 * time.tv_usec;
      if(std::isnan(t0)) {
        t0 = 0;
      }
      double dt = relative ? t - t0 : t;
      t0 = t;
      return dt;
    } else {
      static double t0 = NAN;
      struct rusage usage;
      alert(getrusage(RUSAGE_SELF, &usage) != 0, "illegal-state", "in aidesys::now, bad getrusage() system call");
      double t = 1e+3 * usage.ru_utime.tv_sec + 1e-3 * usage.ru_utime.tv_usec;
      if(std::isnan(t0)) {
        t0 = 0;
      }
      double dt = relative ? t - t0 : t;
      t0 = t;
      return dt;
    }
  }
  std::string nowISO()
  {
    struct timeval timeval_;
    alert(gettimeofday(&timeval_, NULL) != 0, "illegal-state", "in aidesys::nowISO, bad gettimeofday() system call");
    time_t time_t_ = timeval_.tv_sec;
    struct tm *tm_ = localtime(&time_t_);
    char stime[32];
    strftime(stime, 32, "%Y-%m-%dT%H:%M:%S", tm_);
    std::string result = stime;
    return result;
  }
}
