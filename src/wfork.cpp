#include "wfork.hpp"
#include <unistd.h>
#include <sys/wait.h>
#include <set>

namespace aidesys {
  pid_t wfork(std::function < int() > runner, char what, double delay)
  {
    aidesys::alert(what != 't' && what != 'i', "illegal-argument", "in aidesys::wfork what: '%c' id neither 't' for setTimeout, nor 'i' setInterval", what);
    // Here is the very simple implementation of forking a process to obtain  setTimeout / setInterval mechanism
    {
      pid_t pid;
      {
        switch(pid = fork()) {
        case -1:
          wfork_abort(true, "cannot fork the background process");
          break;
        // This code is executed in the child
        case 0:
          while(true) {
            usleep(1000 * delay);
            if(runner() != 0 || what != 'i') {
              wfork_register(getpid(), false); // Internal optional routine to clean child processes at program end
              exit(0);
            }
          }
        // The remainer code is executed in the parent
        default:
          break;
        }
      }
      wfork_register(pid, true); // Internal optional routine to clean child processes at program end
      return pid;
    }
  }
  // This kills a wfork process
  void wfork_kill(int pid)
  {
    // Sends the kill signal
    kill(pid, SIGKILL);
    // Waits until the process effectively stops
    while(waitpid(pid, NULL, 0) == -1) {
      wfork_abort(errno != EINTR, "failed to wait for killed process end");
    }
    wfork_register(pid, false); // Internal routine to clean child processes at program end
  }
  // This registers each process in order to properly kill them at program end.
  static struct Wfork_register_pids {
    std::set < int > pids;
    // At program end, the destructor kill all processes (that's the trick :) …)
    ~Wfork_register_pids() {
      for(auto it = pids.begin(); it != pids.end(); it++) {
        wfork_kill(*it);
      }
    }
    //
    void wfork_register(int pid, bool add_else_del)
    {
      if(add_else_del) {
        pids.insert(pid);
      } else {
        pids.erase(pid);
      }
    }
    // This data structure is present until programs end.
  }
  wfork_register_pids;
  void wfork_register(int pid, bool add_else_del)
  {
    wfork_register_pids.wfork_register(pid, add_else_del);
  }
  // This is the error management: print a message and abort
  void wfork_abort(bool condition, String message)
  {
    if(condition) {
      fprintf(stderr, "Runtime error in aidesys::wfork, %s\n", message.c_str());
      abort();
    }
  }
  // Register created processes to
  //
  std::string wfork(std::function < int() > runner, unsigned int timeout, unsigned int *status)
  {
    // This is a more complex example with managing a timeout and collecting child process stdout
    pid_t timer_pid, background_pid, faster_pid;
    if(timeout > 0) {
      // Forks a timeout process if required
      switch(timer_pid = fork()) {
      case -1:
      {
        wfork_abort(true, "cannot fork the timer process");
        break;
      }
      case 0:
      {
        usleep(timeout * 1000);
        exit(0);
      }
      default:
        break;
      }
    }
    // Constructs the background process with stdout
    int pp[2];
    {
      // Creates a pipe to read the system stdout
      wfork_abort(pipe(pp) == -1, "cannot create pipe");
      // Forks the background process
      switch(background_pid = fork()) {
      case -1:
        wfork_abort(true, "cannot fork the background process");
        break;
      // This code is executed in the child
      case 0:
      {
        // Connects stdout to the pipe
        wfork_abort(dup2(pp[1], 1) != 1, "failed to connect pipe to stdout");
        // Closes unused pipes
        close(pp[0]);
        close(pp[1]);
        // Executes the process in the child
        exit(runner());
      }
      default:
        break;
      }
    }
    // Manages the time out mechanism if any
    {
      // Waits for the faster child process to end
      while((faster_pid = waitpid(WAIT_ANY, (int *) status, 0)) == -1) {
        wfork_abort(errno != EINTR, "failed to wait for 1st process end");
      }
      // Manages if timeout or not
      if(faster_pid == background_pid) {
        // The background child process ends before time, thus no timeout
        if(status != NULL) {
          *status = (*status / 256) & 0xFF;
        }
        if(timeout > 0) {
          // Stops the timer child process
          wfork_kill(timer_pid);
        }
      } else {
        // The timer child process ends first, thus timeout
        if(status != NULL) {
          *status = 256;
        }
        // Stops the background child process in timeout
        wfork_kill(background_pid);
      }
    }
    // Gets the stdout, if any
    {
      // Closes unused pipe descriptor
      close(pp[1]);
      // Reads the stdout
      std::string result;
      for(char c; read(pp[0], &c, 1) == 1;) {
        result += c;
      }
      // Closes and synchronizes
      close(pp[0]);
      // Returns the result
      return result;
    }
  }
}
