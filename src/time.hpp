#ifndef __aidesys_time__
#define __aidesys_time__

#include "std.hpp"

namespace aidesys {
  /**
   * @class time
   * @description Specifies the time management specific functions.
   * - These functions are accessible via the `aidesys::` prefix.
   */
  /**
   * @function sleep
   * @memberof time
   * @static
   * @description Sleeps allowing other threads to proceed.
   * @param {uint} delay The delay in milliseconds.
   */
  void sleep(unsigned int delay);

  /**
   * @function now
   * @memberof time
   * @static
   * @description Gets the cpu or real time from program start.
   * - The following construct measures the calculation time of a given calculation:
   * ```
   *   aidesys::now(false, false);
   *   calculation();
   *   printf("calculus duration in msec : %.3f\n", aidesys::now(false, true));
   * ```
   * @param {bool} [daytime=true] If true considers real time, if false consider cpu time.
   * @param {bool} [relative=false] If true returns the relative time between two calls of now(), else returns either:
   * - The number of milliseconds elapsed since January 1, 1970, 00:00:00 UTC.
   * - The cpu time from program start.
   * @return {double} The time in milli-seconds, with maximal of micro-second precision.
   */
  double now(bool daytime = true, bool relative = false);

  /**
   * @function nowISO
   * @memberof time
   * @static
   * @description Returns the present date and time, with one second precision, following the ISO8601 norm.
   * @return {string} The date and time in the '%Y-%m-%dT%H:%M:%S' format.
   */
  std::string nowISO();
}
#endif
