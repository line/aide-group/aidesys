#ifndef ON_WIN

// Implements the curl http interface

#include "http.hpp"

#include <curl/curl.h>

namespace aidesys {
  class SysHTTPWriter {
public:
    static size_t writeFunction(void *ptr, size_t size, size_t nmemb, std::string *data)
    {
      data->append((char *) ptr, size * nmemb);
      return size * nmemb;
    }
  };
  std::string http(String url, String input, String method)
  {
    CURL *curl = curl_easy_init();
    if(curl) {
      struct curl_slist *headers = NULL;
      curl_easy_setopt(curl, CURLOPT_URL, url.c_str());
      std::string response;
      curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, SysHTTPWriter::writeFunction);
      curl_easy_setopt(curl, CURLOPT_WRITEDATA, &response);
      if(method == "GET") {
        curl_easy_setopt(curl, CURLOPT_CUSTOMREQUEST, "GET");
      } else if(method == "POST") {
        curl_easy_setopt(curl, CURLOPT_POST, 1L);
        curl_easy_setopt(curl, CURLOPT_POSTFIELDS, input.c_str());
      }  else if(method == "PUT") {
        curl_easy_setopt(curl, CURLOPT_PUT, 1L);
        curl_easy_setopt(curl, CURLOPT_POSTFIELDS, input.c_str());
        headers = curl_slist_append(headers, "Content-Type: application/json");
        curl_easy_setopt(curl, CURLOPT_HTTPHEADER, headers);
      } else {
        alert(false, "illegal-argument", "in aidesys::http, undefined HTTP method'" + method + "'");
      }
      CURLcode res = curl_easy_perform(curl);
      curl_easy_cleanup(curl);
      if(headers != NULL) {
        curl_slist_free_all(headers);
      }
      return res == CURLE_OK ? response : aidesys::echo("{ curl_error : %s }", curl_easy_strerror(res));
    } else {
      return "{ curl_error : 'unable to start curl' }";
    }
  }
}
#endif
