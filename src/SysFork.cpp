#include "SysFork.hpp"
#include "wfork.hpp"
#include <unistd.h>
#include <sys/wait.h>

namespace aidesys {
  int SysFork::runner()
  {
    aidesys::alert("illegal-argument", "in aidesys::fork, the runner is not implemented");
    return 0;
  }
#if 1
  std::string SysFork::run()
  {
    return wfork([this]() { return runner();
                 }, timeout, &status);
  }
#else

  /*
   *  void SysFork_alert(bool condition, String message)
   *  {
   *  if(condition) {
   *  fprintf(stderr, "Runtime error in aidesys::fork, %s\n", message.c_str());
   *  exit(-1);
   *  }
   *  }
   *  std::string SysFork::run()
   *  {
   *  int pp[2];
   *  // Creates a pipe to read the system stdout
   *  SysFork_alert(pipe(pp) == -1, "cannot create pipe");
   *  // Forking the system call using the pipe
   *  {
   *  switch(fork()) {
   *  // Error in the forking process
   *  case -1:
   *  SysFork_alert(true, "cannot fork");
   *  break;
   *  // This code is executed in the child
   *  case 0:
   *  {
   *  // Connects stdout to the pipe
   *  SysFork_alert(dup2(pp[1], 1) != 1, "failed to connect pipe to stdout");
   *  // Closes unused pipes
   *  close(pp[0]);
   *  close(pp[1]);
   *  // Executes and quit
   *  exit(runner());
   *  }
   *  default:
   *  // The code remainder is executed by the parent
   *  break;
   *  }
   *  }
   *  wait(NULL);
   *  // Closes unused pipe descriptor
   *  close(pp[1]);
   *  // Reads the stdout
   *  std::string result;
   *  for(char c; read(pp[0], &c, 1) == 1;) {
   *  result += c;
   *  }
   *  // Closes and synchronizes
   *  close(pp[0]);
   *  // Returns the result
   *  return result;
   *  }
   */
#endif
}
