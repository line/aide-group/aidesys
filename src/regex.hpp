#ifndef __aidesys_regex__
#define __aidesys_regex__

#include "std.hpp"
#include <vector>
#include <array>

namespace aidesys {
  /**
   * @class regex
   * @description Specifies the regular expression (regex) specific functions.
   * - The regex pattern is a string with a special syntax that describes what constitutes a match,
   * using the [ECMAScript syntax](http://www.cplusplus.com/reference/regex/ECMAScript).
   * - It appears that some regex catching multiline string may fail or induce infinite loops:
   *    - The solution is to convert a multiline string into a single line string, e.g.: <br/>
   * `aidesys::regexReplace(aidesys::regexReplace(string, "\\s+", " "), regex_for_single_line, output)`
   * - These functions are accessible via the `aidesys::` prefix.
   * - Some multi-line regular expression may induce a "loop forever" bug!
   *   - In that case, such routine is easily called with a timeout, suing a construct of the form:
   * ```
   *  std::string regexReplace(String string, String regex, String output, unsigned int timeout) {
   *    return wfork([string, regex, output]() { cout << aidesys::regexReplace(string, regex, output); return 0; }, timeout);
   *  }
   * ```
   */
  /**
   * @function regexMatch
   * @memberof regex
   * @static
   * @description Regular expression match function.
   * @param {string} string The input string.
   * @param {string} regex The regular-expression to match, following the [ECMAScript syntax](http://www.cplusplus.com/reference/regex/ECMAScript) syntax.
   * @return {bool} True if the _whole_ string match the regex, false otherwise.
   */
  bool regexMatch(String string, String regex);

  /**
   * @function regexIndexes
   * @memberof regex
   * @static
   * @description Regular expression localisation function.
   * @param {string} string The input string.
   * @param {string} regex The regular-expression to match, following the [ECMAScript syntax](http://www.cplusplus.com/reference/regex/ECMAScript) syntax.
   * @return {vector} An `std::vector<std::array<unsigned int, 2>>` of the form `{ {position, length}, ...}` with all matched string position and length.
   */
  std::vector < std::array < unsigned int, 2 >> regexIndexes(String string, String regex);

  /**
   * @function regexReplace
   * @memberof regex
   * @static
   * @description Regular expression replace function.
   * @param {string} string The input string.
   * @param {string} regex The regular-expression to match, following the [ECMAScript syntax](http://www.cplusplus.com/reference/regex/ECMAScript) syntax.
   * @param {string} output The output pattern.
   * @return {string} The string replacements of _all_ matched expression, if any, else the original string.
   */
  std::string regexReplace(String string, String regex, String output);

  /**
   * @function regexSplit
   * @memberof regex
   * @static
   * @description Splits a string as vector of token.
   * @param {string} string The input string.
   * @param {String} regex The regex delimiter.
   * @return {Array<string>} A `std::vector<std::string>` with the string items.
   */
  std::vector < std::string > regexSplit(String string, String regex);
}
#endif
