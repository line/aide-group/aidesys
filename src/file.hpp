#ifndef __aidesys_file__
#define __aidesys_file__

#include "std.hpp"

namespace aidesys {
  /**
   * @class file
   * @description Specifies the file system specific functions.
   * - These functions are accessible via the `aidesys::` prefix.
   */
  /**
   * @function readDir
   * @memberof file
   * @static
   * @description Lists all files of a directory.
   * @param {string} path The directory to list.
   * @param {string} [pattern=".*"] The file pattern regular expression using the [ECMAScript syntax](http://www.cplusplus.com/reference/regex/ECMAScript).
   * @return {Array<string>} A `std::vector<std::string>` structure with the file names.
   * - The file base names with file extension and without the directory path are provided.
   */
  const std::vector < std::string > readDir(String path, String pattern = ".*");

  /**
   * @function getFileTime
   * @memberof file
   * @static
   * @description Gets the file last modificaton time, in milliseconds.
   * @param {string} filename The file path name.
   * @return The file last modificaton time, in milliseconds, since January 1, 1970, midnight, UTC/GMT (i.e., Greewitch time).
   * - Returns NAN, if the file does not exists:
   *    - So: `std::isnan(aidesys::getFileTime(file))` is equivalent to `aidesys::exists(file)`.
   */
  double getFileTime(String filename);

  /**
   * @function path
   * @memberof file
   * @static
   * @description Returns a filename path element.
   * @param {string} filename The file path name.
   * @param {char} [what='b']
   * - 'b' The base-name of the file without extension.
   * - 'f' The file base-name with the extension.
   * - 'e' The extension of the filename.
   * - 'd' The directory name of the filename.
   * - 'n' The normalized path name, resolving the `.` and `..` path elements, and multiple `/`.
   * - 'r' The real absolute normalized path name, taking system existing location into account.
   * @return The filename element.
   */
  std::string path(String filename, char what = 'b');

  /**
   * @function exists
   * @memberof file
   * @static
   * @description Checks if a file exists.
   * @param {string} filename The file path name.
   * @return {bool} True if the file exists and is readable, false otherwise.
   */
  bool exists(String filename);

  /**
   * @function load
   * @memberof file
   * @static
   * @description Loads a textual file.
   * @param {string} filename The file path name.
   * @return {string} The file contents as a string.
   */
  std::string load(String filename);

  /**
   * @function save
   * @memberof file
   * @static
   * @description Saves a textual file.
   * - The file directory is created, if required.
   * - If the file already exists, a backup of name `filename~` is created.
   * @param {string} filename The file path name.
   * @param {string} string The file contents as a string.
   */
  void save(String filename, String string);
}
#endif
