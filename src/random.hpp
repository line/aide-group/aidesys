#ifndef __aidesys_random__
#define __aidesys_random__

#include "std.hpp"

namespace aidesys {
  double random(char what = 'u', unsigned int degree = 1);
}
#endif
