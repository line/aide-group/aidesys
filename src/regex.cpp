#include "regex.hpp"

#include <regex>
#include <sstream>
#include <map>
#include <sys/wait.h>

namespace aidesys {
  class SysRegex {
public:
    static const std::basic_regex < char > &get_regex(String regex) {
      static std::map < std::string, std::basic_regex < char >> regexes;
      auto it = regexes.find(regex);
      if(it == regexes.end()) {
        std::basic_regex < char > r(regex, std::regex_constants::ECMAScript);
        return regexes[regex] = r;
      } else {
        return it->second;
      }
    }

    static String get_error_message(const std::regex_error& error)
    {
      static std::map < unsigned int, std::string > error_messages = {
        { std::regex_constants::error_collate, "The expression contained an invalid collating element name." },
        { std::regex_constants::error_ctype, "The expression contained an invalid character class name." },
        { std::regex_constants::error_escape, "The expression contained an invalid escaped character, or a trailing escape." },
        { std::regex_constants::error_backref, "The expression contained an invalid back reference." },
        { std::regex_constants::error_brack, "The expression contained mismatched brackets ([ and ])." },
        { std::regex_constants::error_paren, "The expression contained mismatched parentheses (( and ))." },
        { std::regex_constants::error_brace, "The expression contained mismatched braces ({ and })." },
        { std::regex_constants::error_badbrace, "The expression contained an invalid range between braces ({ and })." },
        { std::regex_constants::error_range, "The expression contained an invalid character range." },
        { std::regex_constants::error_space, "The memory to convert the expression into a finite state machine overflows." },
        { std::regex_constants::error_badrepeat, "The expression contained a repeat specifier (one of *?+{) that was not preceded by a valid regular expression." },
        { std::regex_constants::error_complexity, "The complexity of an attempted match against a regular expression exceeded a pre-set level." },
        { std::regex_constants::error_stack, "The memory to determine whether the regular expression could match the specified character sequence overflows." }
      };
      return error_messages[error.code()];
    }
  };

  bool regexMatch(String string, String regex)
  {
    try {
      return std::regex_match(string, SysRegex::get_regex(regex));
    }
    catch(std::regex_error& e) {
      alert("illegal-argument", "in aide::sys::regexMatch, spurious regex '" + regex + " : " + SysRegex::get_error_message(e));
      return false;
    }
  }
  std::vector < std::array < unsigned int, 2 >> regexIndexes(String string, String regex) {
    std::vector < std::array < unsigned int, 2 >> results;
    try {
      std::string s = string;
      const std::basic_regex < char >& r = SysRegex::get_regex(regex);
      std::smatch m;
      unsigned int i0 = 0;
      while(std::regex_search(s, m, r)) {
        std::array < unsigned int, 2 > result;
        result[0] = (int) (i0 + m.position());
        result[1] = (int) m.length();
        results.push_back(result);
        unsigned int i1 = m.position() + m.length();
        i0 += i1;
        s = s.substr(i1);
      }
    }
    catch(std::regex_error& e) {
      alert("illegal-argument", "in aide::sys::regexMatch, spurious regex '" + regex + " : " + SysRegex::get_error_message(e));
    }
    return results;
  }

  std::string regexReplace(String string, String regex, String output)
  {
    try {
      std::string result = std::regex_replace(string, SysRegex::get_regex(regex), output, std::regex_constants::match_not_null);
      return result;
    }
    catch(std::regex_error& e) {
      alert("illegal-argument", "in aide::sys::regexReplace, spurious regex '" + regex + "' : " + SysRegex::get_error_message(e));
      return string;
    }
  }
  std::vector < std::string > regexSplit(String string, String regex) {
    if(regex.length() == 1) {
      // Fast implementation for single character split
      std::stringstream stream(string);
      std::string item;
      std::vector < std::string > items;
      while(std::getline(stream, item, regex[0])) {
        items.push_back(item);
      }
      return items;
    } else {
      std::vector < std::string > results;
      std::string s = string;
      const std::basic_regex < char >& r = SysRegex::get_regex(regex);
      std::smatch m;
      while(std::regex_search(s, m, r)) {
        results.push_back(s.substr(0, m.position()));
        s = s.substr(m.position() + m.length());
      }
      if(!s.empty()) {
        results.push_back(s);
      }
      return results;
    }
  }
}
