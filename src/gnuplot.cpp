#include "gnuplot.hpp"

namespace aidesys {
  void gnuplot(String file, String plot, String data)
  {
    // Replaces all $ by \$ in plot
    std::string splot = plot;
    {
      static String from = "$", to = "\\$";
      for(size_t start_pos = 0; (start_pos = splot.find(from, start_pos)) != std::string::npos; start_pos += to.length()) {
        if((start_pos + 5 < splot.length()) && (splot.substr(start_pos + 1, 5) != "title")) {
          splot.replace(start_pos, from.length(), to);
        }
      }
    }
    std::string script =
      "# " + file + "gnuplot display script (automatically generated, do NOT edit)\n" +
      "title=\"title \\\"`basename $0 | sed 's/.gnuplot.sh$//'`\\\"\"\n" +
      "while [ \\! -z \"$1\" ] ; do case \"$1\" in\n" +
      " --png)  line1='set term png'; line2='set output \"" + file + ".png\"';;\n" +
      " *) echo '$0 [--png]'; exit;;\n" +
      "esac; shift; done\n\n" +
      "cat << EOD | gnuplot -persist\n" +
      "$line1\n$line2\n" +
      splot + "\n" +
      "EOD\n";
    save(file + ".gnuplot.sh", script);
    if(data != "") {
      save(file + ".dat", data);
    }
    system(("sh " + file + ".gnuplot.sh --png 2>/dev/null 1>/dev/null").c_str());
  }
}
