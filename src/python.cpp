#ifndef ON_WIN
#ifndef SWIG

// Encapsulates the python wrapper interface
#include "python.hpp"
#include "regex.hpp"
// #include "SysFork.hpp"
#include "wfork.hpp"

namespace aidesys {
  /* Implements a Python wrapper allowing to execute python code from C/C++ */
  class SysPythonWrapper {
    // Ref: https://docs.python.org/3/c-api/index.html with https://docs.python.org/3.7/c-api/veryhigh.html
    wchar_t *python_pprogram = NULL;
public:
    ~SysPythonWrapper() {
      stop();
    }

    // Starts the Python service
    void start()
    {
      if(python_pprogram == NULL) {
        python_pprogram = Py_DecodeLocale(aidesys::argv()[0].c_str(), NULL);
        aidesys::alert(python_pprogram == NULL, "illegal-state", "in aidesys::python/start, cannot decode argv[0]");
        Py_SetProgramName(python_pprogram);
        Py_Initialize();
      }
    }
    // Gets a value as a string, printing the value in the python interpreter and returning stdout
    std::string get(String name, unsigned int timeout)
    {
#if 1
      std::string result = wfork([name]() { aidesys::python("print(" + name + ")");
                                            return 0;
                                 }, timeout, NULL);
#else

      /*
       *  class PrintFork: public SysFork {
       *  std::string name;
       *  public:
       *  PrintFork(String name, unsigned int timeout) : SysFork(timeout), name(name) {}
       *  int runner()
       *  {
       *       aidesys::python("print(" + name + ")");
       *       return 0;
       *  }
       *  };
       *  static PrintFork printFork(name, timeout);
       *  std::string result = printFork.run();
       */
#endif
      result = aidesys::regexReplace(result, "\\n$", "");
      return result;
    }
    // Stops the Python service
    void stop()
    {
      if(python_pprogram != NULL) {
        aidesys::alert(Py_FinalizeEx() < 0, "illegal-state", "in aidesys::python/stop, python finalization failed");
        PyMem_RawFree(python_pprogram);
        python_pprogram = NULL;
      }
    }
  };

  std::string python(String what, unsigned int timeout)
  {
    static SysPythonWrapper sysPythonWrapper;
    if(what == "start") {
      sysPythonWrapper.start();
    } else if(what == "stop") {
      sysPythonWrapper.stop();
    } else if(regexMatch(what, "get .*")) {
      std::string name = regexReplace(what, "get \\s*([^\\s]*)\\s*", "$1");
      return sysPythonWrapper.get(name, timeout);
    } else {
      PyRun_SimpleString(what.c_str());
    }
    return "";
  }
}
#endif
#endif
