#include "std.hpp"
#include "regex.hpp"

#include <stdio.h>
#include <string.h>

// Used by aidesys::alert
#include <stdarg.h>
#include <signal.h>
#ifndef ON_WIN
#include <execinfo.h>
#endif

namespace aidesys {
  const unsigned int message_buffer_size = 10000;

  std::string echo(String message, ...)
  {
    // Parses the message à-la-printf
    char chars_buffer[message_buffer_size];
    {
      va_list a;
      va_start(a, message);
      vsnprintf(chars_buffer, message_buffer_size, message.c_str(), a);
      va_end(a);
    }
    return chars_buffer;
  }
#ifndef ON_WIN
  // Implements the self-debugging backtrace mechanism
  // Ref: https://stackoverflow.com/questions/691719/c-display-stack-trace-on-exception
  class Backtrace {
    // Installs stackback trace on pertinent signals
    static bool init()
    {
      struct sigaction sa;
      {
        sa.sa_handler = bt_sighandler;
        sigemptyset(&sa.sa_mask);
        sa.sa_flags = SA_RESTART;
      }
      std::vector < int > signals = { SIGABRT, SIGALRM, SIGBUS, SIGFPE, SIGHUP, SIGILL, SIGINT, SIGIOT, /*SIGKILL,*/ SIGPIPE, SIGQUIT, SIGSEGV, /* SIGSTOP,*/ SIGTSTP, SIGSYS, SIGTERM, SIGURG, SIGUSR1, SIGUSR2, SIGVTALRM, SIGXCPU, SIGXFSZ };
      for(unsigned int i = 0; i < signals.size(); i++) {
        sigaction(signals[i], &sa, NULL);
      }
      return true;
    }
    static bool initialized;
    // Signal callback for stackback trace
    static void bt_sighandler(int sig)
    {
      static bool already_there = false;
      if(!already_there) {
        already_there = true;
        fprintf(stderr, "Runtime fatal error, recieving the signal %d: '%s'\n", sig, strsignal(sig));
        dump();
      }
      exit(sig);
    }
public:
    // Dumps the backtrace
    static void dump()
    {
#ifndef ON_MAC
      unsigned int size = 32;
      void *trace[size];
      size = backtrace(trace, size);
      char **messages = backtrace_symbols(trace, size);
      for(unsigned int i = 2; i < size; i++) {
        unsigned int j;
        for(j = 0; messages[i][j] != '(' && messages[i][j] != ' ' && messages[i][j] != 0; j++) {}
        std::string s = system(echo("addr2line %p -e %.*s 2>/dev/null", trace[i], j, messages[i]), true);
        // Filters spurious lines and the aidesys::alert source code lines
        if(s != "" && s[0] != '?' && !aidesys::regexMatch(s, "^.*std.cpp:[0-9]*$")) {
          fprintf(stderr, "\t%s\n", s.c_str());
        }
      }
#endif
    }
  };
  bool Backtrace::initialized = Backtrace::init();
#endif
  // Implements a generic exception
  class exception: public std::exception {
    std::string message;
public:
    exception(String message) : message(message) {}
    virtual const char *what() const throw() {
      return message.c_str();
    }
  };

  // Implements the alert mechanism

  void alert_on_message(String thrown, const char *message)
  {
    if(thrown.length() > 0) {
      if(thrown[0] == ' ') {
        // Prints a warning
        fprintf(stderr, "Runtime warning \"%s\": %s\n", &(thrown.c_str()[1]), message);
        if(thrown.length() > 1 && thrown[1] != ' ') {
#ifndef ON_WIN
          Backtrace::dump();
#endif
        }
      } else {
        // Throws an exception
        throw exception(thrown + ": " + message);
      }
    } else {
      // Stdout the notice
      printf("Runtime notice: %s\n", message);
    }
  }
  bool alert(unsigned int condition, String thrown, String message, ...)
  {
    // Here we use an unsigned int and not a bool to avoid a spurious overloading
    if((bool) condition) {
      char chars_buffer[message_buffer_size];
      {
        va_list a;
        va_start(a, message);
        vsnprintf(chars_buffer, message_buffer_size, message.c_str(), a);
        va_end(a);
      }
      alert_on_message(thrown, chars_buffer);
    }
    return condition;
  }
  bool alert(String thrown, String message, ...)
  {
    char chars_buffer[message_buffer_size];
    {
      va_list a;
      va_start(a, message);
      vsnprintf(chars_buffer, message_buffer_size, message.c_str(), a);
      va_end(a);
    }
    alert_on_message(thrown, chars_buffer);
    return true;
  }
  bool alert(unsigned int condition, String thrown)
  {
    // Here we use an unsigned int and not a bool to avoid a spurious overloading
    if((bool) condition) {
      alert_on_message(thrown, "");
    }
    return condition;
  }
  bool alert(String thrown)
  {
    alert_on_message(thrown, "");
    return true;
  }
}
