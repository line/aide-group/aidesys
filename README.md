# aidesys

Basic system C/C++ interface routines to ease multi-language middleware integration

@aideAPI

## Presentation

This factory encapsulates basic system interface.

- Functionalities includes:
  - Standard `std` functions for code string formatting and debugging :
    - `aidesys::echo` To format a string à-la `sprintf`s
    - `aidesys::alert` To verify an assertion and send a warning or a fatal error with trace.
    - `aidesys::argv` To get program line arguments.
    - `aidesys::system` To execute commands at the OS level.
  - Simple files `file` interface:
    - `aidesys::readDir` To returns the contents of a directory
    - `aidesys::getFileTime` and `aidesys::exists` To get the last file modification time and check if a file exists.
    - `aidesys::load` and `aidesys::save` To load/save textual files.
  - Time `time` management:
    - `aidesys::sleep` To introduce a delay.
    - `aidesys::now` To measure the real or CPU relative or absolute time.
    - `aidesys::nowISO` To get the present date and time in a standard format.
  - Regular expression (regex) and string manipulation:
    - `aidesys::regexMatch` and `aidesys::regexReplace` To match and replace strings.
    - `aidesys::regexIndexes` To get all occurrences of a regex in a string.
    - `aidesys::stringSplit` To split a string according to a regex.
  - HTTP interface via the `aidesys::http` function.
  - Python online interface via the `aidesys::python` function.
  - Pseudo-random number generation via the `aidesys::random` function.

- The implementation consists mainly of wrappers to standard third-party libraries.

<a name='what'></a>

## Package repository

- Package files: <a target='_blank' href='https://gitlab.inria.fr/line/aide-group/aidesys'>https://gitlab.inria.fr/line/aide-group/aidesys</a>
- Package documentation: <a target='_blank' href='https://line.gitlabpages.inria.fr/aide-group/aidesys'>https://line.gitlabpages.inria.fr/aide-group/aidesys</a>
- Source files: <a target='_blank' href='https://gitlab.inria.fr/line/aide-group/aidesys/-/tree/master/src'>https://gitlab.inria.fr/line/aide-group/aidesys/-/tree/master/src</a>
- Saved on <a target='_blank' href='https://archive.softwareheritage.org/browse/origin/directory/?origin_url=https://gitlab.inria.fr/line/aide-group/aidesys'>softwareherirage.org</a>
- Version `1.1.1`
- License `CECILL-C`

## Installation

### User simple installation

- `npm install git+https://gitlab.inria.fr/line/aide-group/aidesys.git`

### Co-developper installation

- See the <a target='_blank' href='https://line.gitlabpages.inria.fr/aide-group/aidebuild/install.html#.install_as_developer'>related documentation</a>

Please refer to the <a target='_blank' href='https://line.gitlabpages.inria.fr/aide-group/aidebuild/install.html'>installation guide</a> for installation.

<a name='how'></a>

## Usage

### npm script usage
```
npm install --quiet : installs all package dependencies and sources.
npm run build: builds the different compiled, documentation and test files.
npm test     : runs functional and non-regression tests.
npm run clean: cleans installation files.
```

<a name='dep'></a>

## Dependencies

- None

## devDependencies

- <tt>aidebuild: <a target='_blank' href='https://line.gitlabpages.inria.fr/aide-group/aidebuild'>Builds multi-language compilation packages and related documentation.</a></tt>

<a name='who'></a>

## Author

- Thierry Vieville <thierry.vieville@inria.fr>

