# chisquare0gnuplot display script (automatically generated, do NOT edit)
title="title \"`basename $0 | sed 's/.gnuplot.sh$//'`\""
while [ \! -z "$1" ] ; do case "$1" in
 --png)  line1='set term png'; line2='set output "chisquare0.png"';;
 *) echo '$0 [--png]'; exit;;
esac; shift; done

cat << EOD | gnuplot -persist
$line1
$line2
set title "chisquare0 as normal"
set yrange [0:*] writeback
plot "chisquare0.dat" using 1:2 with boxes linecolor "black" notitle, "chisquare0.dat" using 1:3 with lines linecolor "red" linewidth 2 notitle

EOD
