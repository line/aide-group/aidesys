# normal2gnuplot display script (automatically generated, do NOT edit)
title="title \"`basename $0 | sed 's/.gnuplot.sh$//'`\""
while [ \! -z "$1" ] ; do case "$1" in
 --png)  line1='set term png'; line2='set output "normal2.png"';;
 *) echo '$0 [--png]'; exit;;
esac; shift; done

cat << EOD | gnuplot -persist
$line1
$line2
set style fill empty
plot "normal2.dat" using 1:3:2:6:5:7:xticlabels(8) with candlesticks  whiskerbars linecolor "red" lt 3 notitle, '' using 1:4:4:4:4:7 with candlesticks lt -1 notitle
EOD
